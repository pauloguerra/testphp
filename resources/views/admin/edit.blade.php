<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Atualizar dados de Funcionário') }}
        </h2>
    </x-slot>

    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('admin.update', $user->id) }}">
            @csrf @method('PUT')

            <h2 class="font-bold title-font my-4 text-gray-900 text-lg">Dados Pessoais</h2>

            <div class="flex flex-wrap -m-2">
                <div class="p-2 w-full lg:w-1/3">
                    <div class="relative">
                        <x-label for="name" :value="__('Name')" />
                        <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="$user->name" required autofocus />
                    </div>
                </div>
                <div class="p-2 w-full lg:w-1/3">
                    <div class="relative">
                        <x-label for="email" :value="__('Email')" />
                        <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="$user->email" required />
                    </div>
                </div>
                <div class="p-2 w-full lg:w-1/3">
                    <div class="relative">
                        <x-label for="cpf" :value="__('CPF')" />
                        <x-input id="cpf" class="block mt-1 w-full" type="text" name="cpf" :value="$user->cpf" required />
                    </div>
                </div>

                <div class="p-2 w-full lg:w-1/3">
                    <div class="relative">
                        <x-label for="birth_date" :value="__('Data de Nascimento')" />
                        <x-input id="birth_date" class="block mt-1 w-full" type="text" name="birth_date" :value="$user->birth_date" required />
                    </div>
                </div>

                <div class="p-2 w-full lg:w-1/3">
                    <div class="relative">
                        <x-label for="role_id" :value="__('Cargo')" />
                        <select id="role_id" class="block mt-1 w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="role_id" required>
                            <option value="{{ $user->role->id }}" selected>{{ $user->role->name }}</option>
                            @foreach ($roles as $role)
                                <option value="{{ $role->id }}">{{ $role->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="p-2 w-full lg:w-1/3">
                    <div class="relative">
                        <x-label for="is_admin" :value="__('Administrador')" />
                        <input id="is_admin" class="block mt-1" type="checkbox" name="is_admin" {{ $user->is_admin ? 'checked' : '' }} />
                    </div>
                </div>
            </div>

            <h2 class="font-bold title-font my-4 text-gray-900 text-lg">Endereço</h2>

            <div class="flex flex-wrap -m-2">
                <div class="p-2 w-full lg:w-1/3">
                    <div class="relative">
                        <x-label for="zip_code" :value="__('CEP')" />
                        <x-input id="zip_code" class="block mt-1 w-full" type="text" name="zip_code" :value="$address->zip_code" required />
                    </div>
                </div>

                <div class="p-2 w-full lg:w-1/3">
                    <div class="relative">
                        <x-label for="street" :value="__('Rua')" />
                        <x-input id="street" class="block mt-1 w-full" type="text" name="street" :value="$address->street" required />
                    </div>
                </div>

                <div class="p-2 w-full lg:w-1/3">
                    <div class="relative">
                        <x-label for="number" :value="__('Número')" />
                        <x-input id="number" class="block mt-1 w-full" type="text" name="number" :value="$address->number" />
                    </div>
                </div>

                <div class="p-2 w-full lg:w-1/2">
                    <div class="relative">
                        <x-label for="state" :value="__('Estado')" />
                        <x-input id="state" class="block mt-1 w-full" type="text" name="state" :value="$address->state" required />
                    </div>
                </div>

                <div class="p-2 w-full lg:w-1/2">
                    <div class="relative">
                        <div class="relative">
                            <x-label for="city" :value="__('Cidade')" />
                            <x-input id="city" class="block mt-1 w-full" type="text" name="city" :value="$address->city" required />
                        </div>
                    </div>
                </div>

                <div class="p-2 w-full lg:w-1/2">
                    <div class="relative">
                        <x-label for="neighborhood" :value="__('Bairro')" />
                        <x-input id="neighborhood" class="block mt-1 w-full" type="text" name="neighborhood" :value="$address->neighborhood" required />
                    </div>
                </div>

                <div class="p-2 w-full lg:w-1/2">
                    <div class="relative">
                        <x-label for="complement" :value="__('Complemento')" />
                        <x-input id="complement" class="block mt-1 w-full" type="text" name="complement" :value="$address->complement" />
                    </div>
                </div>
            </div>

            <div class="flex items-center justify-end mt-4">
                <x-button class="ml-4">
                    {{ __('Atualizar') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>

    @push('scripts')
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
        <script>
            $(function() {
                $('input[name="cpf"]').mask('000.000.000-00');
                $('input[name="zip_code"]').mask('00000-000');
                $('input[name="birth_date"]').mask('0000-00-00');
            });
        </script>
        <script>
            $(function() {
                let $form = $(this);

                let $zip_code = $form.find('input[name=zip_code], .zip_code');
                let $street = $form.find('input[name=street], .street');
                let $neighborhood = $form.find('input[name=neighborhood], .neighborhood');
                let $city = $form.find('input[name=city], .city');
                let $state = $form.find('input[name=state], .state');

                function changeZipCode() {
                    let zip_code = $zip_code.val().replace(/[^0-9]/, '');

                    if (zip_code.length == 8) {

                        axios.get('https://brasilapi.com.br/api/cep/v1/' + zip_code).then(function (result) {

                            let address = result.data;

                            $street.val(address.street);
                            $neighborhood.val(address.neighborhood);
                            $city.val(address.city);
                            $state.val(address.state);

                        }).catch(function (e) {

                            $street.val('');
                            $neighborhood.val('');
                            $city.val('');
                            $state.val('');

                        });
                    }
                }

                $zip_code.on('keyup', changeZipCode);
            });
        </script>
    @endpush
</x-app-layout>
