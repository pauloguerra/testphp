<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Alterar Senha') }}
        </h2>
    </x-slot>

    <x-auth-card>
        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('update.user.password', Auth::user()->id) }}">
            @csrf @method('PUT')

            <h2 class="font-bold title-font my-4 text-gray-900 text-lg">Alterar Senha</h2>

            <div class="flex flex-wrap -m-2">
                <div class="p-2 w-full lg:w-1/2">
                    <div class="relative">
                        <x-label for="password" :value="__('Senha')" />
                        <x-input id="password" class="block mt-1 w-full"
                                type="password"
                                name="password"
                                required autocomplete="new-password" />
                    </div>
                </div>

                <div class="p-2 w-full lg:w-1/2">
                    <div class="relative">
                        <x-label for="password_confirmation" :value="__('Confirmar Senha')" />
                        <x-input id="password_confirmation" class="block mt-1 w-full"
                                type="password"
                                name="password_confirmation" required />
                    </div>
                </div>
            </div>

            <div class="flex items-center justify-end mt-4">
                <x-button class="ml-4">
                    {{ __('Confirmar') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-app-layout>
