<?php

namespace App\Http\Requests;

use App\Rules\Cpf;
use App\Rules\ZipCode;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'min:1', 'max:255'],
            'email' => ['required', 'unique:users,email', 'email', 'max:255'],
            'cpf' => ['required', 'unique:users,cpf', 'regex:/^[0-9]{3}\.[0-9]{3}\.[0-9]{3}-[0-9]{2}$/', new Cpf()],
            'birth_date' => ['required', 'date'],
            'role_id' => ['required'],
            'password' => ['required', 'confirmed', Rules\Password::min(8)],
            
            'zip_code' => ['required', 'regex:/^[0-9]{5}-?[0-9]{3}$/', new ZipCode()],
            'street' => ['required'],
            'number' => [],
            'city' => ['required'],
            'state' => ['required'],
            'neighborhood' => ['required'],
            'complement' => [],
        ];
    }
}
