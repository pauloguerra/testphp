<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserRequest;
use App\Models\Role;
use App\Models\Schedule;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('is_admin', false)->get();

        return view('admin.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::all();
        $user = User::find($id);
        $address = $user->addresses->first();

        return view('admin.edit', compact('address', 'roles', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $user = User::find($id);
        $address = $user->addresses->first();

        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'cpf' => $request->cpf,
            'birth_date' => $request->birth_date,
            'role_id' => $request->role_id,
            'is_admin' => $request->is_admin ? true : false,
        ]);

        $address->update([
            'zip_code' => $request->zip_code,
            'street' => $request->street,
            'number' => $request->number,
            'state' => $request->state,
            'city' => $request->city,
            'neighborhood' => $request->neighborhood,
            'complement' => $request->complement,
            'user_id' => $user->id,
        ]);

        return redirect()->route('admin.index')->with('success', 'Funcionário atualizado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::findOrFail($id)->delete();

        return back()->with('success', 'O Funcionário foi excluído.');
    }

    public function schedules(Request $request)
    {
        $admin = Auth::user()->id;

        if ($request->input('date_begin')) {
            $date_begin = $request->input('date_begin');
            $date_end = $request->input('date_end');

            $schedules = DB::select("
                SELECT e.*, m.name as manager, schedule_user.*, schedules.*, roles.name as role_name
                FROM users e
                INNER JOIN schedule_user ON e.id = schedule_user.user_id
                INNER JOIN schedules ON schedules.id = schedule_user.schedule_id
                INNER JOIN roles ON roles.id = e.role_id
                INNER JOIN users m ON m.id = e.created_by
                WHERE e.created_by = '$admin' AND
                e.deleted_at IS NULL AND
                schedules.opened_at BETWEEN '$date_begin' AND '$date_end'
                ORDER BY schedules.id DESC
            ");

            return view('schedules.index', compact('schedules'));
        };

        $schedules = DB::select("
            SELECT e.*, m.name as manager, schedule_user.*, schedules.*, roles.name as role_name
            FROM users e
            INNER JOIN schedule_user ON e.id = schedule_user.user_id
            INNER JOIN schedules ON schedules.id = schedule_user.schedule_id
            INNER JOIN roles ON roles.id = e.role_id
            INNER JOIN users m ON m.id = e.created_by
            WHERE e.created_by = '$admin' AND e.deleted_at IS NULL
            ORDER BY schedules.id DESC
        ");

        return view('schedules.index', compact('schedules'));
    }
}
