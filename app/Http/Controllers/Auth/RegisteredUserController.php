<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdatePasswordRequest;
use App\Models\Address;
use App\Models\Role;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $roles = Role::all();

        return view('auth.register', compact('roles'));
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(StoreUserRequest $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'cpf' => $request->cpf,
            'birth_date' => $request->birth_date,
            'role_id' => $request->role_id,
            'is_admin' => $request->is_admin ? true : false,
            'password' => Hash::make($request->password),
            'created_by' => Auth::user()->id,
        ]);

        Address::create([
            'zip_code' => $request->zip_code,
            'street' => $request->street,
            'number' => $request->number,
            'state' => $request->state,
            'city' => $request->city,
            'neighborhood' => $request->neighborhood,
            'complement' => $request->complement,
            'user_id' => $user->id,
        ]);

        event(new Registered($user));

        return redirect(RouteServiceProvider::HOME)->with('success', 'Usuário cadastrado.');
    }

    public function edit()
    {
        return view('auth.edit');
    }

    public function update(UpdatePasswordRequest $request, User $user)
    {
        $user = User::find($user->id);

        $user->update([
            'password' => Hash::make($request->password),
        ]);

        return redirect()->route('dashboard')->with('success', 'A senha foi alterada.');
    }
}
