<?php

namespace App\Services;

use GuzzleHttp\Client;

class AddressService
{
    /**
     * Find address by zip code.
     *
     * @param string $zipCode
     * @return array
     */
    public function addressByZipCode($zipCode)
    {
        $zipCode = trim(str_replace(['.', '-', ' '], '', $zipCode));

        $address = $this->getAddressFromBrasilAPI($zipCode);

        return $address;
    }

    public function getAddressFromBrasilAPI($zipCode)
    {
        try {
            $guzzle = new Client();

            $response = $guzzle->get('https://brasilapi.com.br/api/cep/v1/' . $zipCode, [
                'headers' => [
                    'content-type' => 'application/json;charset=utf-8',
                ],
            ]);

            $result = json_decode($response->getBody()->getContents());

            return [
                'zip_code' => $zipCode,
                'street' => data_get($result, 'street'),
                'neighborhood' => data_get($result, 'neighborhood'),
                'city' => data_get($result, 'city'),
                'state' => data_get($result, 'state'),
            ];
        } catch (\Exception $e) {

        }
    }
}
