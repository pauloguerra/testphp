<?php

namespace App\Rules;

use App\Services\AddressService;
use Illuminate\Contracts\Validation\Rule;

class ZipCode implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ((new AddressService())->addressByZipCode($value)) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'O CEP informado é inválido.';
    }
}
