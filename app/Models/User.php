<?php

namespace App\Models;

use App\Models\Relations\BelongsToManySchedules;
use App\Models\Relations\BelongsToRole;
use App\Models\Relations\HasManyAddresses;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use BelongsToManySchedules;
    use BelongsToRole;
    use HasFactory, Notifiable;
    use HasManyAddresses;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'cpf',
        'password',
        'role_id',
        'birth_date',
        'created_by',
        'is_admin',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'birth_date' => 'date',
    ];

    /**
     * Related admin that created this user.
     *
     * @return BelongsTo|Builder|User
     */
    public function manager()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    /**
     * Get the user's age.
     *
     * @return int
     */
    public function getAgeAttribute()
    {
        return $this->birth_date->diffInYears(Carbon::now());
    }
}
