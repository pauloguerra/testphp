<?php

namespace App\Models\Relations;

use App\Models\Schedule;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

trait BelongsToManySchedules
{
    /**
     * Related schedules.
     *
     * @return BelongsToMany|Builder|Schedule[]|Schedule
     */
    public function schedules()
    {
        return $this->belongsToMany(Schedule::class);
    }
}
