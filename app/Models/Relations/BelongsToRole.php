<?php

namespace App\Models\Relations;

use App\Models\Role;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait BelongsToRole
{
    /**
     * Related role.
     *
     * @return BelongsTo|Builder|Role
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }
}
