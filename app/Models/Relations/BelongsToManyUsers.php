<?php

namespace App\Models\Relations;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

trait BelongsToManyUsers
{
    /**
     * Related users.
     *
     * @return BelongsToMany|Builder|User[]|User
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
