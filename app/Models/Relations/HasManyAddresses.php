<?php

namespace App\Models\Relations;

use App\Models\Address;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait HasManyAddresses
{
    /**
     * Represents a database relationship.
     *
     * @return HasMany|Builder|Address[]|Address
     */
    public function addresses()
    {
        return $this->hasMany(Address::class);
    }
}
