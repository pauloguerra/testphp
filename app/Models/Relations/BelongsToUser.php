<?php

namespace App\Models\Relations;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait BelongsToUser
{
    /**
     * Related user.
     *
     * @return BelongsTo|Builder|User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
