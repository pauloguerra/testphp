<?php

namespace App\Models\Relations;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait HasManyUsers
{
    /**
     * Represents a database relationship.
     *
     * @return HasMany|Builder|User[]|User
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }
}
