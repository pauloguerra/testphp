<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\ScheduleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');
Route::resource('schedule', ScheduleController::class)->middleware('auth');
Route::resource('admin', AdminController::class)->middleware('admin');
Route::get('users/schedules', [AdminController::class, 'schedules'])->name('admin.schedules');

require __DIR__.'/auth.php';
